# Host: localhost  (Version 5.7.14)
# Date: 2018-10-10 14:26:56
# Generator: MySQL-Front 6.0  (Build 3.4)


#
# Structure for table "chamado"
#

CREATE TABLE `chamado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` varchar(255) NOT NULL DEFAULT '',
  `titulo` varchar(255) NOT NULL DEFAULT '',
  `menssagem` json DEFAULT NULL,
  `status` binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

#
# Structure for table "cliente"
#

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `dtNascimento` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
