$(document).ready(function(){

  //var socket = io('http://localhost:3000');
//
  //socket.on('receivedMessage',function (id) {
  //    iniciarChat(id);
  //});

  var idFuncionario = $('#idFuncionario').val();
  var html = '';
  var act = (idFuncionario) ? 'listarTodos' : 'listarChamado';

    $.ajax({
        url: act,
        data: {
            idCliente: $('#idCliente').val()
        },
        dataType: 'json',
        method: "post",
        success: function (data) {
            if(data){

              $(data).each(function(k,v){
                var obj = jQuery.parseJSON(v.menssagem);
                if($.isArray(v.menssagem)){alert('oi');}

                // alert(obj.menssagem);
                if(idFuncionario){
                  html += '<tr onclick="getChat('+v.id+')">';
                  html += '    <td>'+v.email+'</td>';
                  html += '    <td>'+v.titulo+'</td>';
                  html += '    <td>'+obj.menssagem+'</td>';
                  html += '</tr>';
                
              }else{
                  html += '<tr onclick="getChat('+v.id+')">';
                  html += '    <td>'+v.titulo+'</td>';
                  html += '    <td>'+obj.menssagem+'</td>';
                  html += '</tr>';
              }
            });
              $('#chamadosAbertos').html(html);
            }else{
               alert('erro ao cadastrar o chamado');
            }
        }
    });
});

$('#campoMenssagem').on('keypress',function(e){
  if(e.keyCode == 13){
    $('#enviar').trigger('click');
  }
});

$('#enviar').on('click',function(){
  var menssagem = $('#campoMenssagem').val();
  var idCliente = $('#idCliente').val();
  var id        = $('.msg_history').attr('data-id');
  var idFuncionario = $('#idFuncionario').val();
  var act = (idFuncionario) ? 'appendCacheFunc' : 'appendCache';

  $.ajax({
    url: act,
    data: {
        idCliente: idCliente,
        menssagem: menssagem,
        id:id,
        idFuncionario,idFuncionario
    },
    dataType: 'text',
    method: "post",
    success: function (data) {
      //var socket = io('http://localhost:3000');
//
      //$('#campoMenssagem').val('');
      //socket.emit('sendMessage', id);
      iniciarChat(id);
    }
  });

});

$( "#frmCadChamado" ).submit(function( ev ) {
var html = '';
ev.preventDefault();
    $.ajax({
        url: "inserirChamado",
        data: $('#frmCadChamado').serialize(),
        dataType: 'json',
        method: "post",
        success: function (data) {
          if(data){
            var obj = jQuery.parseJSON(data[':menssagem']);

            html += '<tr onclick="getChat('+data['id']+')">';
            html += '    <td>'+data[':titulo']+'</td>';
            html += '    <td>'+obj.menssagem+'</td>';
            html += '</tr>';
            $('#chamadosAbertos').append(html);
            iniciarChat(data['id']);
          }else{
             alert('erro ao cadastrar o chamado');
          }
     }
    });
});

function getChat(id,ajax) {
  $.ajax({
      url: "getChat",
      data: {
          id:id
      },
      dataType: 'json',
      method: "post",
      success: function (data) {
          iniciarChat(id);
      }
  });
    
}

function iniciarChat(id) {

  var html  = '';
  var idMsg = $('.msg_history').attr('data-id',id);

  $.ajax({
        url: "iniciarChat",
        data: {
            id:id
        },
        dataType: 'json',
        method: "post",
        success: function (data)
        {
            var idCliente = $('#idCliente').val();

            $('#ocorrencia').removeClass('hide');
            $('.msg_history').attr('data-id', id);

            $(data).each(function (k, v)
            {
                if (v.idCliente == idCliente)
                {
                    html += '<div class="incoming_msg">';
                    html += '    <div class="received_msg">';
                    html += '       <div class="received_withd_msg">';
                    html += '           <p>' + v.menssagem + '</p>';
                    html += '       </div>';
                    html += '   </div>';
                    html += '</div>';
                } else
                {
                    html += '<div class="outgoing_msg">';
                    html += '    <div class="sent_msg">';
                    html += '        <p>' + v.menssagem + '</p>';
                    //html += '        <span class="time_date"></span>';
                    html += '    </div>';
                    html += '</div>';
                }
                $('.msg_history').html(html);
            });
        }
    });

    setTimeout(function(){ iniciarChat(id); }, 3000);
}