$(document).ready(function(){
    var socket = io('http://localhost:3000');
    
    socket.on('receivedMessage',function (id) {
        iniciarChat(id);
    });
    var html = '';
    $.ajax({
        url: "app/ajax/cadastroChamado.php?action=listarTodos",
        data: {
            idCliente: $('#idCliente').val()
        },
        dataType: 'json',
        method: "post",
        success: function (data) {
            if(data){
                $(data).each(function(k,v){
                   html += '<tr onclick="getChat('+v.id+')">';
                   html += '    <td>'+v.idcliente+'</td>';
                   html += '    <td>'+v.titulo+'</td>';
                   html += '    <td>'+v.menssagem+'</td>';
                   html += '</tr>';

                   $('#chamadosAbertos').html(html);
               });
            }else{
               alert('erro ao cadastrar o chamado');
            }
     }
    });
});

$('#enviarFunc').on('click',function(){
  var menssagem     = $('#campoMenssagem').val();
  var idFuncionario = $('#idFuncionario').val();
  var id            = $('.msg_history').attr('data-id');

  $.ajax({
    url: "app/ajax/cadastroChamado.php?action=appendCacheFunc",
    data: {
        idFuncionario: idFuncionario,
        menssagem: menssagem,
        id:id
    },
    dataType: 'text',
    method: "post",
    success: function (data) {
      iniciarChat(id);
    }
  });
});
