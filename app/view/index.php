<!DOCTYPE html>
<html>
<body>
	<section class="login-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 login-sec">
					<h2 class="text-center" >Logar no sistema</h2>
					<form class="login-form" id="frmLogin">
						<div class="form-group">
							<label for="exampleInputEmail1" class="text-uppercase">E-mail</label>
							<input type="text" name="emailCli" class="form-control">

						</div>
						<div class="form-group">
							<label for="exampleInputPassword1"  class="text-uppercase">Senha</label>
							<input type="password" class="form-control" name="senhaCli">
						</div>


						<div class="form-check col-md-12 center">
							<button id="cadastrar" class="btn btn-login float-left">
								Login
							</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</section>
</body>
<script src="../../js/cadastroCliente.js"></script>
</html>