	<!DOCTYPE html>
	<html>
	<body>
		<section id="cadChamado">
			<div class="cmd-12 login-sec">
				<input type="hidden" id="idCliente" value="<?php echo $_SESSION['idCliente'] ?>">
				<h2 class="text-center">Abrir chamado</h2>
				<form id="frmCadChamado" action="app/ajax/abrirChamado.php" method="post" class="col-md-offset-5">					
					<div>
						<label>
							Titulo:
						</label>
						<input type="text" name="titulo">
					</div>
					<div>
						<label>
							Descrição:
						</label>
						<input name="descricao" >
					</div>
					<button type="submit" class="btn">
						cadastrar ocorrência
					</button>
				</form>
			</div>
		</section>
		<section id="chamado">
			<h2>visualizar chamados</h2>
			<table class="table">
				<thead class="thead-dark">
					<th>Título</th>
					<th>Menssagem</th>
				</thead>
				<tbody id="chamadosAbertos"></tbody>
			</table>
		</section>
		<section id="ocorrencia" class="messaging hide">
			<div class="mesgs">
				<div class="msg_history">
				</div>
				<div class="type_msg">
					<div class="input_msg_write">
						<input type="text" class="write_msg" placeholder="menssagem" id="campoMenssagem">
						<button class="msg_send_btn" type="button" id="enviar"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
					</div>
				</div>
			</div>
		</section>
		<script src="js/cadastroChamado.js"></script>
		</body>
		</html>

