<?php
namespace app\model;

use app\model\dbConnection;

class clienteModel{
    
    private $cliente = null;
    private $conexao = null;

    public function __construct($controller) {
        try {
            $this->cliente = $controller;
            $this->conexao = new dbConnection();

        } catch (Exception $e) {
            throw new Exception("Não foi possível instânciar o objeto cliente model.");
        }
    }

    public function loginCliente()
    {
        try
        {
            $email = $this->cliente->getEmail();
            $senha = $this->cliente->getSenha();

            $sql = 'SELECT id FROM madeiramadeira.cliente WHERE email = :email AND senha = :senha';
            $pdo = $this->conexao->pdo->prepare($sql);

            $pdo->bindParam(':email',$email);
            $pdo->bindParam(':senha',$senha);

            if($pdo->execute()){
               return $pdo->fetch();
            }

        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}

?>
