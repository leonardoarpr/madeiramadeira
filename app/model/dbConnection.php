<?php

namespace app\model;

class dbConnection{

    private $host = 'localhost';
	private $username = 'root';
	private $password = 'root';
        
	public $pdo = '';	
	public $sql;	

	public function getSql() {
	    return $this->sql;
	}

	public function setSql($sql) {
	    $this->sql = $sql;
	    return $this;
	}		

	function __construct(){
        try {
            return $this->pdo = new \PDO( "mysql:host=$this->host;charset=utf8",$this->username, $this->password );
        }catch( PDOException $e ){
            throw new Exception  ( "Não foi possível conectar ao banco de dados" );
        }
	}

	public function prepare($query, $options = NULL) {
	    return $this->pdo->prepare($query,$options);
	} 
    static public function pdoDebugger($sql, $aCamposValores){
        $keys = array();
        $values = $aCamposValores;

        foreach ($values as $key => $value) {

            if (is_string($key)) {
                $keys[] = '/' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }

            if (is_string($value)) {
                $values[$key] = $value;
            } elseif (is_array($value)) {
                $values[$key] = implode(',', $value);
            } elseif (is_null($value)) {
                $values[$key] = 'NULL';
            }
        }

        $sql = preg_replace($keys, $values, $sql, 1, $count);

        return $sql;
    }
}
