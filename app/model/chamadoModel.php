<?php
namespace app\model;

use app\model\dbConnection;

class chamadoModel{
    
    private $chamado = null;
    private $conexao = null;

    public function __construct($controller)
    {
        try {
            $this->chamado = $controller;
            $this->conexao = new dbConnection();
        } catch (Exception $e) {
            throw new Exception("Não foi possível instânciar o objeto chamado model.");
        }
    }

    public function insertChamado()
    {
        try
        {
            $sql = 'INSERT INTO madeiramadeira.chamado(idCliente,titulo,menssagem)
                    VALUES (:idCliente,:titulo,:menssagem)';

            $pdo = $this->conexao->pdo->prepare($sql);

            $aCamposValores[':idCliente'] = $this->chamado->getIdCliente();
            $aCamposValores[':titulo']    = $this->chamado->getTitulo();
            $aCamposValores[':menssagem'] = json_encode(array('idCliente' => $this->chamado->getIdCliente(),
                                                              'menssagem'=> nl2br($this->chamado->getMenssagem())));


            if($pdo->execute($aCamposValores))
            {
                $aCamposValores['id'] =  $this->conexao->pdo->lastInsertId();
                return $aCamposValores;
            }

        }catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function listarChamados()
    {
        try
        {
            $idCliente = $this->chamado->getIdCliente();

            $sql = 'SELECT id,idCliente,titulo,menssagem FROM madeiramadeira.chamado WHERE idCliente = :idCliente AND status = 1';

            $pdo = $this->conexao->pdo->prepare($sql);
            $pdo->bindParam(':idCliente', $idCliente);

            if($pdo->execute())
            {
                return $pdo->fetchAll();
            }

        }catch (PDOException $e){

            echo $e->getMessage();
        }
    }

    public function listaTodos()
    {
        try
        {
            $sql = 'SELECT c.id,c.idCliente,c.titulo,c.menssagem,i.email FROM madeiramadeira.chamado c
                    INNER JOIN madeiramadeira.cliente i ON i.id = c.idCliente
                    WHERE status = 1';

            $pdo = $this->conexao->pdo->prepare($sql);
            if($pdo->execute())
            {
                return $pdo->fetchAll();
            }
        }catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getChat()
    {
        try
        {
            $id = $this->chamado->getId();

            $sql = 'SELECT id,idCliente,titulo,menssagem FROM madeiramadeira.chamado WHERE id = :id';

            $pdo = $this->conexao->pdo->prepare($sql);
            $pdo->bindParam(':id', $id);

            if($pdo->execute())
            {
                return $pdo->fetch();
            }
        }catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

}

?>
