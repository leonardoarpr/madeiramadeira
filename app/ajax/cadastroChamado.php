<?php
require '../../common/php/autoload.php';
require '../../bibliotecas/redis/redis.php';
namespace app

class ajax
{
	function __construct()
	{
		extract($_POST);

		switch ($_GET['action']) {
		case 'listarTodos':
		$objChamado = new chamadoController;
		echo json_encode($objChamado->listaTodos());
		break;
		case 'listarChamado':
		$objChamado = new chamadoController;
		$objChamado->setIdCliente($idCliente);

		echo json_encode($objChamado->listarChamados());
		break;
		case 'inserirChamado':
		$objChamado = new chamadoController;
		$objChamado->setIdCliente($_SESSION['idCliente']);
		$objChamado->setTitulo($titulo);
		$objChamado->setMenssagem($descricao);

		echo json_encode($objChamado->insertChamado());
		break;
		case 'getChat':
		$objChamado = new chamadoController;
		$objChamado->setId($id);
		echo json_encode($objChamado->getChat());
		break;
		case 'appendCache':
		$redis = new redis;
		$cache = $redis->redis->get('id'.$id);
		$cache = str_replace(']','',$cache);

		echo $redis->redis->set('id'.$id,$cache.','.json_encode(array('idCliente' => $idCliente,'menssagem' => $menssagem)).']');
		break;
		case 'appendCacheFunc':
		$redis = new redis;
		$cache = $redis->redis->get('id'.$id);
		$cache = str_replace(']','',$cache);

		echo $redis->redis->set('id'.$_POST['id'],$cache.','.json_encode(array('idFuncionario' => $idFuncionario,'menssagem' => $menssagem)).']');
		break;
		case 'iniciarChat':
		$redis = new redis;
		echo $redis->redis->get('id'.$id);
		break;
	}
	}	
}
?>