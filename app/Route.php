<?php 
namespace app;

use config\init\bootstrap;
use app\controller\ajaxChamadoController;

session_start();

class Route extends bootstrap
{
	
	protected function initRoutes()
	{
	    // MENUS
		$routes['home']     = array('route' => '/', 'controller' => 'indexController', 'action' => 'index');
		$routes['login']    = array('route' => '/cadastroCliente', 'controller' => 'indexController', 'action' => 'cadastroCliente');
        $routes['chamado']  = array('route' => '/chamado', 'controller' => 'indexController', 'action' => 'chamado');
        $routes['restrito'] = array('route' => '/restrito', 'controller' => 'indexController', 'action' => 'restrito');
        $routes['sair']     = array('route' => '/sair', 'controller' => 'indexController', 'action' => 'sair');

        // CHAMDO CLIENTE
        $routes['inserirChamado'] = array('route' => '/inserirChamado', 'controller' => 'ajaxChamadoController', 'action' => 'inserirChamado');
        $routes['listarChamado']  = array('route' => '/listarChamado', 'controller' => 'ajaxChamadoController', 'action' => 'listarChamado');
        $routes['iniciarChat']    = array('route' => '/iniciarChat', 'controller' => 'ajaxChamadoController', 'action' => 'iniciarChat');
        $routes['getChat']        = array('route' => '/getChat', 'controller' => 'ajaxChamadoController', 'action' => 'getChat');
        $routes['appendCache']    = array('route' => '/appendCache', 'controller' => 'ajaxChamadoController', 'action' => 'appendCache');

        // CHAMADO FUNCIONARIO
        $routes['listarTodos']     = array('route' => '/listarTodos', 'controller' => 'ajaxChamadoController', 'action' => 'listarTodos');
        $routes['appendCacheFunc'] = array('route' => '/appendCacheFunc', 'controller' => 'ajaxChamadoController', 'action' => 'appendCacheFunc');

		$this->setRoute($routes);
	}
}