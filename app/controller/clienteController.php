<?php
namespace app\controller;

use app\model\clienteModel;
use config\controller\action;

class clienteController extends action{

    private $id;
    private $nome;
    private $dtNascimento;
    private $email;
    private $senha;
    
    public function getId(){
        return $this->id;
    }

    public function setId(int $id){
        $this->id = $id;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setNome(string $nome){
        $this->nome = $nome;
    }

    public function getDtNascimento(){
        return $this->dtNascimento;
    }

    public function setDtNascimento(date $dtNascimento){
        $this->dtNascimento = $dtNascimento;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail(string $email){
        $this->email = $email;
    }

    public function getSenha(){
        return $this->senha;
    }

    public function setSenha(string $senha){
        $this->senha = $senha;
    }

    public function getDataCadastro(){
        return $this->dataCadastro;
    }

    public function loginCliente(){
        try {
            $objImportacaoModel = new clienteModel($this);
            if($result = $objImportacaoModel->loginCliente()){
                $_SESSION['idCliente'] = $result['id'];
                return $result['id'];
            }
            
        } catch (PDOException $e) {
            throw new Exception('Não foi possível executar o comando.');
        }

    }
}

?>