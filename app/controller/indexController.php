<?php
namespace app\controller;

use config\controller\action;
use config\init\bootstrap;
use app\controller\clienteController;

class indexController extends action
{
	public function index()
	{
		$this->render('index');
	}

	public function chamado()
	{
		$this->render('chamado');
	}

	public function restrito()
	{
		$this->render('restrito');
	}

	public function sair()
	{
        session_destroy();
		$this->render('index');
	}

	public function cadastroCliente()
	{
        $objCliente = new clienteController;
        $objCliente->setEmail($_POST['emailCli']);
        $objCliente->setSenha($_POST['senhaCli']);
        echo ($objCliente->loginCliente());
    }

}
