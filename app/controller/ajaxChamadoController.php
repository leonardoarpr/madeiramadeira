<?php
namespace app\controller;

use app\controller\chamadoController;
use app\biblioteca\redis\redis;

class ajaxChamadoController
{
    public function inserirChamado()
    {
		$objChamado = new chamadoController;
		$objChamado->setIdCliente($_SESSION['idCliente']);
		$objChamado->setTitulo($_POST['titulo']);
		$objChamado->setMenssagem($_POST['descricao']);
		echo json_encode($objChamado->inserirChamado());
    }

    public function listarChamado()
    {
        $objChamado = new chamadoController;
        $objChamado->setIdCliente($_POST['idCliente']);

        echo json_encode($objChamado->listarChamados());
    }

    public function listarTodos()
    {
        $objChamado = new chamadoController;
        echo json_encode($objChamado->listaTodos());
    }

    public function getChat()
    {
        $objChamado = new chamadoController;
        $objChamado->setId($_POST['id']);
        echo json_encode($objChamado->getChat());
    }

    public function iniciarChat()
    {
        $redis = new redis;
        echo $redis->redis->get('id' . $_POST['id']);
    }

    public function appendCache()
    {
        $redis = new redis;
        $cache = $redis->redis->get('id'.$_POST['id']);
        $cache = str_replace(']','',$cache);

        echo $redis->redis->set('id'.$_POST['id'],$cache.','.json_encode(array('idCliente' => $_POST['idCliente'],'menssagem' => $_POST['menssagem'])).']');
    }

    public function appendCacheFunc()
    {
        $redis = new redis;
        $cache = $redis->redis->get('id'.$_POST['id']);
        $cache = str_replace(']','',$cache);

        echo $redis->redis->set('id'.$_POST['id'],$cache.','.json_encode(array('idFuncionario' => $_POST['idFuncionario'],'menssagem' => $_POST['menssagem'])).']');
    }
}
?>