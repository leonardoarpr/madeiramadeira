<?php
namespace app\controller;

use app\model\chamadoModel;
use app\biblioteca\redis\redis;

class chamadoController
{

    private $id;
    private $idCliente;
    private $titulo;
    private $menssagem;
    
    public function getId(){
        return $this->id;
    }

    public function setId(int $id){
        $this->id = $id;
    }

    public function getIdCliente(){
        return $this->idCliente;
    }

    public function setIdCliente(int $idCliente){
        $this->idCliente = $idCliente;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setTitulo(string $titulo){
        $this->titulo = $titulo;
    }

    public function getMenssagem(){
        return $this->menssagem;
    }

    public function setMenssagem(string $menssagem){
        $this->menssagem = $menssagem;
    }

    public function inserirChamado() :array
    {
        try {
            $objChamadoModel = new chamadoModel($this);
            if($result = $objChamadoModel->insertChamado()){
                return $this->chamadoCache($result);
            }
    } catch (PDOException $e) {
            throw new Exception('Não foi possível executar o comando.');
        }

    }

    public function listarChamados() :array
    {
        try {
            $objChamadoModel = new chamadoModel($this);
            return $objChamadoModel->listarChamados();
        } catch (PDOException $e) {
            throw new Exception('Não foi possível executar o comando.');
        }

    }

    public function listaTodos() :array
    {
        try {
            $objChamadoModel = new chamadoModel($this);
            return $objChamadoModel->listaTodos();
    } catch (PDOException $e) {
            throw new Exception('Não foi possível executar o comando.');
        }

    }

    public function getChat() :array
    {
        try {
            $redis = new redis();
            if($redis->redis->exists('id'.$this->getId())){
                return $redis->redis->get('id'.$this->getId());
            }
            $objChamadoModel = new chamadoModel($this);
            if($result = $objChamadoModel->getChat()){
                return $this->chamadoCache($result);
            }
        } catch (PDOException $e) {
            throw new Exception('Não foi possível executar o comando.');
        }

    }

    public function chamadoCache($menssagem) :array
    {
        $redis = new redis;
        $redis->redis->set('id'.$menssagem['id'],'['.(!empty($menssagem[':menssagem']) ? $menssagem[':menssagem'] : $menssagem['menssagem']).']');
        return $menssagem;
    }

}

?>
