const express = require('express');
const path =  require('path');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
let port = 3000

var phpExpress = require('php-express')({
    binPath: 'php'
});

app.use(express.static(path.join(__dirname,'public')));
app.set('views',path.join(__dirname,'public'));
//app.engine('html',require('ejs').renderFile);socket
app.engine('php', phpExpress.engine);
app.set('view engine', 'php');

//app.all(/.+\.php$/, phpExpress.router);
//app.get('/', function(req, res){
    //res.send('<h1>Hello world</h1>');
//});

io.on('connection',socket => {
	console.log(`listening on port ${port}`);
    socket.on('sendMessage',data => {
        socket.broadcast.emit('receivedMessage', data);
    });
});

server.listen(port);