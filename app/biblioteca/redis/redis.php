<?php
namespace app\biblioteca\redis;

require_once 'vendor/autoload.php';

class redis
{
	public $redis;
    public function __construct()
    {
        return $this->redis = new \Predis\Client(
        	['scheme' => 'tcp',
            'host' => 'localhost',
            'port' => 6379, // WINDOWS
        ]);
    }
}
?>
