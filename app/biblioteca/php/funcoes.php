<?php

/* * *
 * Classe de funções auxiliares
 */

class Funcoes
{ 

    /**
     *
     * @param type $formataReal
     * @return string
     */
    public static function formataReal($formataReal)
    {
        if ($formataReal) {
            $modificado = 'R$ ' . number_format((float)$formataReal, 2, ',', '.');
            return $modificado;
        } else {
            if (floatval($formataReal) == 0 || intval($formataReal) == 0) {
                return 'R$ 0,00';
            }
        }
    }

    public static function formataRealCusto($formataReal)
    {
        if ($formataReal) {
            $modificado = 'R$ ' . number_format((float)$formataReal, $_SESSION['USUARIO']['casas_decimais'], ',', '.');
            return $modificado;
        } else {
            if (floatval($formataReal) == 0 || intval($formataReal) == 0) {
                return 'R$ 0,00';
            }
        }
    }    

    /**
     * @param $formataReal
     * @return string
     */
    public static function formataRealToNumber($formataReal)
    {
        return str_replace(',', '.', str_replace('.', '', str_replace('R$', '', $formataReal)));
    }

    /**
     *
     * @param type $dataAmericana
     * @return string
     */
    public static function datetoBR($dataAmericana)
    {
        if ($dataAmericana) {
            $d = explode('-', $dataAmericana);
            $dtOK = $d[2] . '/' . $d[1] . '/' . $d[0];
            return $dtOK;
        }
    }

    public static function datetoBRfromUSdatetime($dateTimeAmericana)
    {
        $d = explode(' ', $dateTimeAmericana);
        $dtOK = Funcoes::datetoBR($d[0]);
        return $dtOK;
    }

    public static function datetoUS($dataBrasil)
    {
        $d = explode('/', $dataBrasil);
        $d[2] = empty($d[2]) ? '' : $d[2];
        $d[1] = empty($d[1]) ? '' : $d[1];
        $d[0] = empty($d[0]) ? '' : $d[0];
        $dtOK = $d[2] . '-' . $d[1] . '-' . $d[0];
        return $dtOK;
    }

    public static function dateTimeToBR($data_americana_his)
    {
        $d = explode(' ', $data_americana_his);
        $d[0] = empty($d[0]) ? '' : $d[0];
        $d[1] = empty($d[1]) ? '' : $d[1];
        $ok = Funcoes::datetoBR($d[0]) . ' ' . $d[1];
        return $ok;
    }

    public static function dateTimeToBR2($data_americana_his)
    {
        $d = explode(' ', $data_americana_his);
        $ok = Funcoes::datetoBR($d[0]) . ' ' . $d[1];
        return $ok;
    }

    public static function dateTimeToDateBR($data_americana_his)
    {
        $d = explode(' ', $data_americana_his);
        $ok = Funcoes::datetoBR($d[0]);
        return $ok;
    }

    public static function dateTimeToUS($data_br_his)
    {
        $d = explode(' ', $data_br_his);
        $d[0] = empty($d[0]) ? '' : $d[0];
        $d[1] = empty($d[1]) ? '' : $d[1];
        $ok = Funcoes::datetoUS($d[0]) . ' ' . $d[1];
        return $ok;
    }

    public static function dateTimeToUS2($data_br_his)
    {
        $d = explode(' ', $data_br_his);
        $ok = Funcoes::datetoUS($d[0]) . ' ' . $d[1];
        return $ok;
    }

    // Coloca mascara de CPF 00000000000 para 000.000.000-00
    public static function mascaraCpf($p_cpf)
    {
        if ($p_cpf) {
            $p_cpf = str_pad($p_cpf, 11, 0, STR_PAD_LEFT);
            $a = substr($p_cpf, 0, 3);
            $b = substr($p_cpf, 3, 3);
            $c = substr($p_cpf, 6, 3);
            $d = substr($p_cpf, 9, 2);
            $cpf_view = $a;
            $cpf_view .= ".";
            $cpf_view .= $b;
            $cpf_view .= ".";
            $cpf_view .= $c;
            $cpf_view .= "-";
            $cpf_view .= $d;
            return ($cpf_view);
        }
    }

    // Coloca mascara de CNPJ 00000000000000 para 00.000.000/0000-00
    public static function mascaraCnpj($p_cnpj)
    {
        if ($p_cnpj) {
            $p_cnpj = str_pad($p_cnpj, 14, 0, STR_PAD_LEFT);
            $a = substr($p_cnpj, 0, 2);
            $b = substr($p_cnpj, 2, 3);
            $c = substr($p_cnpj, 5, 3);
            $d = substr($p_cnpj, 8, 4);
            $e = substr($p_cnpj, 12, 2);
            $cnpj_view = $a;
            $cnpj_view .= ".";
            $cnpj_view .= $b;
            $cnpj_view .= ".";
            $cnpj_view .= $c;
            $cnpj_view .= "/";
            $cnpj_view .= $d;
            $cnpj_view .= "-";
            $cnpj_view .= $e;
            return ($cnpj_view);
        }
    }

    /**
     * Coloca mascara de CEP 00000000 para 00.000-000
     * @param type $p_cep
     * @return type
     */
    public static function mascaraCep($p_cep)
    {
        if ($p_cep) {
            $p_cep = str_pad($p_cep, 8, 0, STR_PAD_LEFT);
            $a = substr($p_cep, 0, 2);
            $b = substr($p_cep, 2, 3);
            $c = substr($p_cep, 5, 3);
            $cep_view = $a;
            //$cep_view .= ".";
            $cep_view .= $b;
            $cep_view .= "-";
            $cep_view .= $c;
            return ($cep_view);
        }
    }

    /**
     * M�scara Documentos (CPF e CNPJ)
     * @param type $cpfcnpj
     * @return type
     */
    public static function mascaraDoc($cpfcnpj)
    {

        $cpfcnpj = str_replace('.', '', $cpfcnpj);
        $cpfcnpj = str_replace('-', '', $cpfcnpj);
        $cpfcnpj = str_replace('/', '', $cpfcnpj);

        if (strlen($cpfcnpj) <= 11)
            return funcoes::mascaraCpf($cpfcnpj);
        else
            return funcoes::mascaraCnpj($cpfcnpj);
    }

    /**
     * M�scara Documentos (CPF e CNPJ)
     * @param type $cpfcnpj
     * @return type
     */
    public static function somenteDoisZerosDecimais($valor)
    {

        return $valor = number_format(floatval($valor), 2, '.', '');
    }

    /**
     * M�scara Telefone
     * @param type $p_fone
     * @return type
     */
    public static function mascaraFone($p_fone)
    {
        if ($p_fone) {
            if (strlen($p_fone) == 10)
                return '(' . substr($p_fone, 0, 2) . ') ' . substr($p_fone, 2, 4) . '-' . substr($p_fone, 6, 4);
            else
                return '(' . substr($p_fone, 0, 2) . ') ' . substr($p_fone, 2, 5) . '-' . substr($p_fone, 7, 4);
        }
    }

    /**
     * M�scara Ag�ncia
     * @param type $p_agencia
     * @return type
     */
    public static function mascaraAgencia($p_agencia)
    {
        $p_agencia = str_replace('-', '', $p_agencia);

        if (strlen($p_agencia) == 4)
            return $p_agencia;
        else
            return substr($p_agencia, 0, 4) . '-' . substr($p_agencia, 4, 1);
    }

    /**
     * M�scara Conta
     * @param type $p_cta
     * @return type
     */
    public static function mascaraConta($p_cta)
    {
        $p_cta = str_replace('-', '', $p_cta);
        return substr($p_cta, 0, strlen($p_cta) - 1) . '-' . substr($p_cta, strlen($p_cta) - 1, 1);
    }

    /**
     * Monta Options de combo normal c�digo e descri��o
     * @param type $selecionado
     * @param type $arrayDeDados
     */
    public static function montaCombo($selecionado, $arrayDeDados)
    {
        /**
         * Verifica se $arrayDeDados é um array
         */
        if (is_array($arrayDeDados)) {
            /**
             * Percorre array de dados
             */
            foreach ($arrayDeDados as $indice => $valor) {
                /**
                 * Valor
                 */
                $value = $valor [0];
                /**
                 * Descrição
                 */
                $descricao = $valor [1];
                /**
                 * Criando options
                 */
                echo "<option value='" . $value . "'";
                /**
                 * Se $selecionado igual a $value seleciona
                 */
                echo $selecionado == $value ? ' selected' : '';
                /*
                 * Vizualizando Permissão
                 */
                echo ">" . $descricao . "</option>";
            }
            /**
             * Se array de dados não foi passado
             */
        } else {
            /**
             * Cria option com mensagem de erro
             */
            echo '<option>Não foi possível carregar, array de dados  não foi enviado.</option>';
        }
    }

    public static function montaComboFabricante($selecionado, $arrayDeDados)
    {
        /**
         * Verifica se $arrayDeDados é um array
         */
        if (is_array($arrayDeDados)) {
            /**
             * Percorre array de dados
             */
            foreach ($arrayDeDados as $indice => $valor) {
                /**
                 * Valor
                 */
                $value = $valor [1];
                /**
                 * Descrição
                 */
                $descricao = $valor [1];
                /**
                 * Criando options
                 */
                echo "<option value='" . $value . "'";
                /**
                 * Se $selecionado igual a $value seleciona
                 */
                echo $selecionado == $value ? ' selected' : '';
                /*
                 * Vizualizando Permissão
                 */
                echo ">" . $descricao . "</option>";
            }
            /**
             * Se array de dados não foi passado
             */
        } else {
            /**
             * Cria option com mensagem de erro
             */
            echo '<option>Não foi possível carregar, array de dados  não foi enviado.</option>';
        }
    }

    /**
     * Monta com enviando como value o valor
     * @param type $selecionado
     * @param type $aTipoDeLograDouros
     */
    public static function montaComboValor($selecionado, $aArray)
    {
        if (is_array($aArray)) {
            foreach ($aArray as $indice => $valor) {
                $value = $valor [0];
                echo "<option value='" . $value . "'";
                echo $selecionado == $value ? ' selected' : '';
                echo ">" . $value . "</option>";
            }
        } else {
            echo '<option>Não foi possível carregar, array de dados  não foi enviado.</option>';
        }
    }

    /**
     * Monta com enviando como value o a descri��o e na label também mostra descri��o
     * @param type $selecionado
     * @param type $aTipoDeLograDouros
     */
    public static function montaComboValorValor($selecionado, $aTipoDeLograDouros)
    {
        if (is_array($aTipoDeLograDouros)) {
            foreach ($aTipoDeLograDouros as $indice => $valor) {
                $value = $valor [1];
                $descricao = $valor [1];
                echo "<option value='" . $value . "'";
                echo $selecionado == $value ? ' selected' : '';
                echo ">" . $descricao . "</option>";
            }
        } else {
            echo '<option>Não foi possível carregar, array de dados  não foi enviado.</option>';
        }
    }

    /**
     * Monta combo enviando com valor(value) o c�digo e na label mostra c�digo - descri��o
     * @param type $selecionado
     * @param type $aTipoDeLograDouros
     */
    public static function montaComboValorCodigoDescricao($selecionado, $aTipoDeLograDouros)
    {
        if (is_array($aTipoDeLograDouros)) {
            foreach ($aTipoDeLograDouros as $indice => $valor) {
                $value = $valor [0];
                $descricao = $valor [1];
                echo "<option value='" . $value . "'";
                echo $selecionado == $value ? ' selected' : '';
                echo ">" . $value . ' - ' . $descricao . "</option>";
            }
        } else {
            echo '<option>Não foi possível carregar, array de dados  não foi enviado.</option>';
        }
    }

    /**
     * Retira M�scara de Telefone
     * @param type $telefone
     * @return type
     */
    public static function retiraMascaraTelefone($telefone)
    {
        $telefone = str_replace('(', '', $telefone);
        $telefone = str_replace(') ', '', $telefone);
        $telefone = str_replace(')', '', $telefone);
        $telefone = str_replace('-', '', $telefone);
        return $telefone;
    }

    /**
     * Retira M�scara de CPF e CNPJ
     * @param type $cnpjCpf
     * @return type
     */
    public static function retiraMascaraCnpjCpf($cnpjCpf)
    {
        if (strlen($cnpjCpf) == 14) {
            $cnpjCpf = str_replace('.', '', $cnpjCpf);
            $cnpjCpf = str_replace('-', '', $cnpjCpf);
            return $cnpjCpf;
        } else {
            $cnpjCpf = str_replace('.', '', $cnpjCpf);
            $cnpjCpf = str_replace('/', '', $cnpjCpf);
            $cnpjCpf = str_replace('-', '', $cnpjCpf);
            return $cnpjCpf;
        }
    }

    /**
     * Retira M�scara Moeda
     * @param type $valor
     * @return type
     */
    public static function retiraMascaraMoeda($valor)
    {
        $valor = str_replace('R$', '', $valor);
        $valor = str_replace(' ', '', $valor);
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        return $valor;
    }
    
//     public static function formataMoedaSql($valor)
//     {
//     	$valor = str_replace('.', '', $valor);
//     	$valor = str_replace(',', '.', $valor);
//     	return $valor;
//     }

    /**
     * Retira M�scara Rg
     * @param type $rg
     * @return type
     */
    public static function retiraMascaraRg($rg)
    {
        $rg = str_replace('.', '', $rg);
        $rg = str_replace('-', '', $rg);
        return $rg;
    }

    /**
     * Grava texto no banco de dados exatamente da forma que vem do input
     * @param type $texto
     * @return type
     */
    public static function formataTexto($texto)
    {
        // Order of replacement
        $str = $texto;
        $order = array("\r\n", "\n", "\r");
        $replace = '<br />';
        // Processes \r\n's first so they aren't converted twice.
        return $newstr = str_replace($order, $replace, $str);
    }

    /**
     * Retira Acentos
     * @param type $string
     * @return string
     */
    public static function retiraAcentos($texto)
    {

        // Array caracteres a serem substitu�dos
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
        , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", "*", "(", ")");
        // Array de substituicao
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
        , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "", "", "");
        // Retorno
        return str_replace($array1, $array2, $texto);
    }

    /* public static function retiraAcentos2($texto) {

      // Array caracteres a serem substitu�dos
      $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
      , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç");
      // Array de substituicao
      $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
      , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C");
      // Retorno
      return str_replace($array1, $array2, $texto);
      } */

    /**
     * Retira '-' do CEP
     * @param type $cep
     * @return string
     */
    public static function retiraMascaraCep($cep)
    {
        return str_replace('-', '', $cep);
    }

    /**
     * Valida CNPJ
     * @param string $cnpj
     * @return bool true para CNPJ correto
     *
     */
    public static function validaCnpj($cnpj)
    {
        // Deixa o CNPJ com apenas números
        $cnpj = preg_replace('/[^0-9]/', '', $cnpj);
        // Garante que o CNPJ é uma string
        $cnpj = (string)$cnpj;
        // O valor original
        $cnpj_original = $cnpj;
        // Captura os primeiros 12 números do CNPJ
        $primeiros_numeros_cnpj = substr($cnpj, 0, 12);

        /**
         * Multiplica��o do CNPJ
         * @param string $cnpj Os digitos do CNPJ
         * @param int $posicoes A posicao que vai iniciar a regressão
         * @return int O
         *
         */
        function multiplica_cnpj($cnpj, $posicao = 5)
        {
            // Variável para o cálculo
            $calculo = 0;

            // Laço para percorrer os item do cnpj
            for ($i = 0; $i < strlen($cnpj); $i++) {
                // Cálculo mais posi��o do CNPJ * a posi��o
                $calculo = $calculo + ($cnpj[$i] * $posicao);

                // Decrementa a posi��o a cada volta do laço
                $posicao--;

                // Se a posi��o for menor que 2, ela se torna 9
                if ($posicao < 2) {
                    $posicao = 9;
                }
            }
            // Retorna o cálculo
            return $calculo;
        }

        // Faz o primeiro cálculo
        $primeiro_calculo = multiplica_cnpj($primeiros_numeros_cnpj);

        // Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
        // Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
        $primeiro_digito = ($primeiro_calculo % 11) < 2 ? 0 : 11 - ($primeiro_calculo % 11);

        // Concatena o primeiro dígito nos 12 primeiros números do CNPJ
        // Agora temos 13 números aqui
        $primeiros_numeros_cnpj .= $primeiro_digito;

        // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posi��o 6
        $segundo_calculo = multiplica_cnpj($primeiros_numeros_cnpj, 6);
        $segundo_digito = ($segundo_calculo % 11) < 2 ? 0 : 11 - ($segundo_calculo % 11);

        // Concatena o segundo dígito ao CNPJ
        $cnpj = $primeiros_numeros_cnpj . $segundo_digito;

        // Verifica se o CNPJ gerado é idêntico ao enviado
        if ($cnpj === $cnpj_original) {
            return true;
        }
    }

    /**
     * Escreve valor por extenso
     * @param type $valor
     * @param type $bolExibirMoeda
     * @param type $bolPalavraFeminina
     * @return string
     */
    public static function valorPorExtenso($valor = 0, $bolExibirMoeda = true, $bolPalavraFeminina = false)
    {

        $valor = self::removerFormatacaoNumero($valor);

        $singular = null;
        $plural = null;

        if ($bolExibirMoeda) {
            $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
        } else {
            $singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("", "", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
        }

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");

        if ($bolPalavraFeminina) {

            if ($valor == 1) {
                $u = array("", "uma", "duas", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
            } else {
                $u = array("", "um", "duas", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
            }
            $c = array("", "cem", "duzentas", "trezentas", "quatrocentas", "quinhentas", "seiscentas", "setecentas", "oitocentas", "novecentas");
        }
        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);

        for ($i = 0; $i < count($inteiro); $i++) {
            for ($ii = mb_strlen($inteiro[$i]); $ii < 3; $ii++) {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }

        // $fim identifica onde que deve se dar jun��o de centenas por "e" ou por "," ;)
        $rt = null;
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++;
            elseif ($z > 0)
                $z--;

            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];

            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        $rt = mb_substr($rt, 1);

        return ($rt ? trim($rt) : "zero");
    }

    /**
     * Remove formata��o de valores
     * @param type $strNumero
     * @return string
     */
    public static function removerFormatacaoNumero($strNumero)
    {

        $strNumero = trim(str_replace("R$", null, $strNumero));

        $vetVirgula = explode(",", $strNumero);
        if (count($vetVirgula) == 1) {
            $acentos = array(".");
            $resultado = str_replace($acentos, "", $strNumero);
            return $resultado;
        } else if (count($vetVirgula) != 2) {
            return $strNumero;
        }

        $strNumero = $vetVirgula[0];
        $strDecimal = mb_substr($vetVirgula[1], 0, 2);

        $acentos = array(".");
        $resultado = str_replace($acentos, "", $strNumero);
        $resultado = $resultado . "." . $strDecimal;

        return $resultado;
    }

    /**
     * Retorna o valor da Consulta enviada baseado no ARRAY
     * @param type $arraydados , $codcons
     * @return string
     */
    public static function retornaValorConsulta($arraydados, $codcons)
    {
        if ($arraydados) {
            foreach ($arraydados as $key => $value) {
                $cod = $value['codcons'];
                if ($cod == $codcons)
                    echo number_format($value['valorcons'], 2, ',', '.');
            }
        }
    }

    /**
     * Retorna o valor da Consulta enviada baseado no ARRAY
     * @param type $arraydados , $codcons
     * @return string
     */
    public static function retornaLimiteConsulta($arraydados, $codcons)
    {
        foreach ($arraydados as $key => $value) {
            $cod = $value['tpcons'];
            if ($cod == $codcons) {
                $qtd = $value['qtd'];
                $consumo = $value['consumo'];
                $sobra = $qtd - $consumo;
                if ($sobra <= 0)
                    return 'class="btn btn-primary right disabled " title=" Limite de Consulta Excedida "';
                else
                    return 'class="btn btn-primary right"';
            }
        }
    }

    public static function retornaLimiteConsultaMensagem($arraydados, $codcons)
    {
        if ($arraydados != '') {
            foreach ($arraydados as $key => $value) {
                $cod = $value['tpcons'];
                if ($cod == $codcons) {
                    $qtd = $value['qtd'];
                    $consumo = $value['consumo'];
                    $sobra = $qtd - $consumo;
                    if ($sobra <= 0)
                        return ' (Limite Excedido) ';
                    else
                        return '';
                }
            }
        }
    }

    public static function retornaLimiteConsultadisable($arraydados, $codcons)
    {
        if ($arraydados != '') {
            foreach ($arraydados as $key => $value) {
                $cod = $value['tpcons'];
                if ($cod == $codcons) {
                    $qtd = $value['qtd'];
                    $consumo = $value['consumo'];
                    $sobra = $qtd - $consumo;
                    if ($sobra <= 0)
                        return ' disabled ';
                    else
                        return '';
                }
            }
        }
    }

    /**
     * M�s por extenso, par�metro m�s em ingl�es
     * @param type $month
     * @return string
     */
    public static function mesPorExtenso($month)
    {
        /**
         * Array de de chaves (ingl�s) e valores (m�s em portugu�s)
         */
        $mes_extenso = array(
            'Jan' => 'Janeiro',
            'Feb' => 'Fevereiro',
            'Mar' => 'Marco',
            'Apr' => 'Abril',
            'May' => 'Maio',
            'Jun' => 'Junho',
            'Jul' => 'Julho',
            'Aug' => 'Agosto',
            'Nov' => 'Novembro',
            'Sep' => 'Setembro',
            'Oct' => 'Outubro',
            'Dec' => 'Dezembro'
        );
        /**
         * Retorn o m�s em portugu�s
         */
        return $mes_extenso [$month];
    }

    /**
     * Returns the emulated SQL string
     *
     * @param $sql
     * @param $parameters
     * @return mixed
     */
    static public function pdoDebugger($sql, $aCamposValores)
    {
        /**
         * Declara��o de vari�veis
         */
        $keys = array();
        $values = $aCamposValores;
        /**
         * Perceorrendo array de valores
         */
        foreach ($values as $key => $value) {
            /**
             * Check if named parameters (':param') or anonymous parameters ('?') are used
             */
            if (is_string($key)) {
                $keys[] = '/' . $key . '/';
            } else {
                $keys[] = '/[?]/';
            }
            /**
             * bring parameter into human-readable format
             */
            if (is_string($value)) {
                $values[$key] = "'" . $value . "'";
            } elseif (is_array($value)) {
                $values[$key] = implode(',', $value);
            } elseif (is_null($value)) {
                $values[$key] = 'NULL';
            }
        }
        /**
         * Substituindo no comando sql
         */
        $sql = preg_replace($keys, $values, $sql, 1, $count);
        /**
         * Retorno
         */
        return $sql;
    }

    /**
     * Verifica se � data
     * @param type $data
     * @return boolean
     */
    public static function validaData($data)
    {
        /**
         * Fatia a string $dat em pedados, usando / como refer�ncia e cria um array
         */
        $data = explode("/", "$data");
        /**
         * Separa o array
         */
        $d = isset($data[0]) ? $data[0] : '';
        $m = isset($data[1]) ? $data[1] : '';
        $y = isset($data[2]) ? $data[2] : '';
        /**
         * Verifica se existem os par�metros
         */
        if ($m && $d && $y) {
            /**
             * Verifica se a data � v�lida!
             */
            $res = checkdate($m, $d, $y);
            /**
             * 1 = true (v�lida)
             */
            if ($res == 1) {
                /**
                 * Retorno
                 */
                return 1;
                /**
                 * 0 = false (inv�lida)
                 */
            } else {
                /**
                 * Retorno
                 */
                return 0;
            }
            /**
             * Se n�o existir os par�mentros
             */
        } else {
            /**
             * Retorno
             */
            return 0;
        }
    }

    /*
     * gera mascaras em php
     *
     * mask($cnpj,'##.###.###/####-##');
     * mask($cpf,'###.###.###-##');
     * mask($cep,'#####-###');
     * mask($data,'##/##/####');
     *
     * @param $val string que receberá a mascara
     * @param $mask string da mascara desejada
     * @return string com a mascara já colocada
     */

    public static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /*
     * remove pontuação de cpf e cnpj
     */

    public static function removePonCPFCNPJ($val)
    {
        $aPontuacao = array('.', '-', '/', " ");
        $val = str_replace($aPontuacao, '', $val);
        return $val;
    }

    /*
     * Retira o numero que vem junto com o nome da caracteristica da grade do produto
     */

    public static function removeNumeroCaracteristicaGrade($caracteristicas)
    {

        if (strripos(',', $caracteristicas) != false) {

            //quebra todas as caracteristicas por bloco de nome caracteristica e seu valor
            $listaCarac = $nome = explode(',', $caracteristicas);
            //se o produto tiver mais de uma caracteristica, faz um loop percorrendo as caracteristicas

            $caracteristicas = '';

            foreach ($listaCarac AS $row) {
                $tipo = explode('-', $row);
                //retira os numeros do nome da caracteristica
                $caracteristicas .= preg_replace("/[^a-zA-Z]+/", "", $tipo[0]) . ':' . $tipo[1] . ' ';
            }
        } else {

            $tipo = explode('-', $caracteristicas);

            $caracteristicas = preg_replace("/[^a-zA-Z]+/", "", $tipo[0]) . ':' . $tipo[1] . ' ';
        }

        return $caracteristicas;
    }

    public static function removeCaracteristicasNomeProd($nomeComCarac)
    {
        if (!strpos($nomeComCarac, ' - ') === false) {
            $posicaoNome = strpos($nomeComCarac, ' - ');

            $nomeComCarac = substr($nomeComCarac, 0, $posicaoNome + 3);
        }

        return $nomeComCarac;
    }

    public static function somenteCaracteristicasNomeProd($nomeComCarac)
    {
        if (!strpos($nomeComCarac, ' - ') === false) {
            $posicaoNome = strpos($nomeComCarac, ' - ');

            $nomeComCarac = str_replace(substr($nomeComCarac, 0, $posicaoNome + 1), '', $nomeComCarac);
        }

        return $nomeComCarac;
    }

    public static function grava_dados_clientes_usuarios($nCpfcgc, $Tipo, $nome, $data_nascimento, $numero_titulo, $endereco, $id_tipo_log, $numero, $complemento, $bairro, $cidade, $uf, $cep, $email, $nome_mae, $telefone, $celular, $referencia_comercial, $referencia_pessoal, $empresa_trabalha, $cargo, $endereco_empresa, $rg, $nome_referencia, $fax, $fone_empresa, $codloja, $fonte)
    {

        $objConexao = new DbConnection();

        if ($nCpfcgc == 0 || empty($nCpfcgc)) {
            $sql_erro = "INSERT INTO cs2.erros (cod_erro, desc_erro) values(NOW(),'$nCpfcgc, $Tipo, $nome, $data_nascimento, $numero_titulo, $endereco, $id_tipo_log, $numero, $complemento, $bairro, $cidade, $uf, $cep, $email, $nome_mae, $telefone, $celular, $referencia_comercial, $referencia_pessoal, $empresa_trabalha, $cargo, $endereco_empresa, $rg, $nome_referencia, $fax, $fone_empresa,$codloja,$fonte')";
            $pdo = $objConexao->pdo->prepare($sql_erro);
            //var_dump($pdo);
            $pdo->execute();
            return;
        }

        $sql_nome = "SELECT Nom_Nome FROM base_inform.Nome_Brasil
                 WHERE Nom_CPF = '$nCpfcgc' AND Nom_Tp= $Tipo AND Origem_Nome_id = 1";
        $pdo = $objConexao->pdo->query($sql_nome);
        $pdo->execute();
        $aResult = $pdo->fetchAll(PDO::FETCH_ASSOC);
        if (count($aResult) == 0) {

            #Verificando se existe o NOME CADASTRADO para o CPF
            $sql_nome = "SELECT Nom_Nome FROM base_inform.Nome_Brasil WHERE Nom_CPF = '$nCpfcgc' AND Nom_Tp= $Tipo AND Origem_Nome_id <> 1 AND Nom_Nome = '$nome' ";
            $pdo = $objConexao->pdo->query($sql_nome);
            $pdo->execute();
            $aResult = $pdo->fetchAll(PDO::FETCH_ASSOC);
            if (count($aResult) == 0) {

                $sql = "INSERT INTO base_inform.Nome_Brasil(Origem_Nome_id, Nom_CPF, Nom_Tp, Nom_Nome, Dt_Cad)
                    VALUES('2','$nCpfcgc','$Tipo','$nome', NOW())";
                $pdo = $objConexao->pdo->prepare($sql);
                $pdo->execute();
            }
        }

        #VERIFICANDO SE EXISTE O REGISTRO OU O ENDERE�O
        if (!empty($endereco)) {
            if ($nCpfcgc > 0) {
                $sql = 'SELECT id  FROM base_inform.Endereco
                    WHERE CPF="'.$nCpfcgc.'" AND logradouro = "'.$endereco.'" AND numero = "'.$numero.'"';
                $pdo = $objConexao->pdo->query($sql);

                $pdo->execute();
                $aResult = $pdo->fetch(PDO::FETCH_ASSOC);

                if (!empty($aResult)) {
                    $sql = "DELETE FROM base_inform.Endereco
                        WHERE
                            id = " . $aResult['id'];
                    $pdo = $objConexao->pdo->prepare($sql);
                    $pdo->execute();
                }

                #NAO EXISTE REGISTRO OU O LOGRADOURO � NOVO, CADASTRAR UM NOVO
                $sql1 = 'INSERT INTO base_inform.Endereco
                            (CPF, Tipo, Origem_Nome_id, Tipo_Log_id, logradouro, numero, complemento, bairro, cidade, uf, cep, data_cadastro)
                        VALUES
                            (
                                "'.$nCpfcgc.'", "'.$Tipo.'", "2", "'.$id_tipo_log.'", "'.$endereco.'", "'.$numero.'", "'.$complemento.'", "'.$bairro.'", "'.$cidade.'", "'.$uf.'", "'.$cep.'", NOW()
                            )
                        ';

                $pdo1 = $objConexao->pdo->prepare($sql1);
                $pdo1->execute();
            }
        }

        //Email
        if (!empty($email)) {
            $sql = "SELECT CPF FROM base_inform.Email_Brasil WHERE CPF = '$nCpfcgc' AND Email = '$email'";
            $pdo = $objConexao->pdo->query($sql);
            $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
            if (empty($aResult)) {
                $sql_insert = " INSERT INTO base_inform.Email_Brasil(CPF, Tipo, Email)
                            VALUES('$nCpfcgc' , '$Tipo' , '$email' )";
                $pdo = $objConexao->pdo->prepare($sql_insert);
                $pdo->execute();
            }
        }

        //RG
        if (!empty($rg)) {
            $sql = "SELECT CPF FROM base_inform.Nome_RG WHERE CPF = '$nCpfcgc'";
            $pdo = $objConexao->pdo->query($sql);
            $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
            if (empty($aResult)) {
                $sql_insert = " INSERT INTO base_inform.Nome_RG(CPF, Tipo, Numero_RG)
                            VALUES('$nCpfcgc' , '$Tipo' , '$rg' )";
                $pdo = $objConexao->pdo->prepare($sql_insert);
                $pdo->execute();
            }
        }

        //Nome_Mae
        if (!empty($nome_mae)) {
            $sql = "SELECT CPF FROM base_inform.Nome_Mae WHERE CPF = '$nCpfcgc'";
            $pdo = $objConexao->pdo->query($sql);
            $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
            if (empty($aResult)) {
                $sql_insert = " INSERT INTO base_inform.Nome_Mae(CPF, Tipo, Nome_Mae)
                            VALUES('$nCpfcgc' , '$Tipo' , '$nome_mae' )";
                $pdo = $objConexao->pdo->prepare($sql_insert);
                $pdo->execute();
            }
        }

        //Data Nascimento
        if ($data_nascimento != '0000-00-00' && $data_nascimento != '') {
            $sql = "SELECT CPF FROM base_inform.Nome_DataNascimento WHERE CPF = '$nCpfcgc'";
            $pdo = $objConexao->pdo->query($sql);
            $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
            if (empty($aResult)) {
                $sql_insert = " INSERT INTO
                                base_inform.Nome_DataNascimento(CPF, Tipo, data_nascimento)
                            VALUES('$nCpfcgc' , '$Tipo' , '$data_nascimento' )";
                $pdo = $objConexao->pdo->prepare($sql_insert);
                $pdo->execute();
            }
        }

        //Titulo de Eleitor
        if (!empty($numero_titulo)) {
            if ($numero_titulo > 0) {
                $sql = "SELECT CPF FROM base_inform.Nome_Titulo WHERE CPF = '$nCpfcgc'";
                $pdo = $objConexao->pdo->query($sql);
                $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
                if (empty($aResult)) {
                    $sql_insert = " INSERT INTO
                                    base_inform.Nome_Titulo(CPF, Tipo, Numero_Titulo)
                                VALUES( '$nCpfcgc' , '$Tipo' , '$numero_titulo' )";
                    $pdo = $objConexao->pdo->prepare($sql_insert);
                    $pdo->execute();
                }
            }
        }

        //Profissao
        if (!empty($empresa_trabalha)) {
            $sql = "SELECT CPF FROM base_inform.Nome_Empresa_Trabalha
                WHERE CPF = '$nCpfcgc' AND Tipo = '$Tipo' AND empresa = '$empresa_trabalha'";
            $pdo = $objConexao->pdo->query($sql);
            $pdo->execute();
            $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
            if (empty($aResult)) {
                $sql_insert = " INSERT INTO base_inform.Nome_Empresa_Trabalha(cnpj_empresa, CPF, Tipo, empresa, cargo, endereco_empresa)
                            VALUES('{$_SESSION['session_insc']}

    ', '$nCpfcgc', '$Tipo', '$empresa_trabalha', '$cargo', '$endereco_empresa')";
                $pdo = $objConexao->pdo->prepare($sql_insert);
                $pdo->execute();
            }
        }

        //Referencia Pessoal
        if (!empty($nome_referencia)) {
            $sql = "SELECT CPF FROM base_inform.Nome_PessoaContato
                WHERE cpf = '$nCpfcgc' AND nome_contato = '$nome_referencia'";
            $pdo = $objConexao->pdo->query($sql);
            $pdo->execute();
            $aResult = $pdo->fetch(PDO::FETCH_ASSOC);
            if (empty($aResult)) {
                $sql_insert = "INSERT INTO base_inform.Nome_PessoaContato(cpf , nome_contato)
                           VALUES('$nCpfcgc', '$nome_referencia')";
                $pdo = $objConexao->pdo->prepare($sql_insert);
                $pdo->execute();
            }
        }
    }

    /**
     * Busca dados na tabela clientes, se nao achar busca na geral
     * @return dados
     */
    public static function dadosPorCpfCnpj($cpf_cnpj_prin)
    {

        $cpf_cnpj = Funcoes::retiraMascaraCnpjCpf(Funcoes::mascaraDoc($cpf_cnpj_prin));
        $conexao = new dbConnection();
        $sql = "
                (SELECT nome, telefone, id_tipo_log AS id_tipo_log
                    , endereco, numero, complemento, cep, bairro, cidade
                    , uf, cnpj_cpf AS cpf
                    , data_cadastro, 'client' AS tipo, rg, fone_empresa, celular
                FROM base_web_control.cliente
                WHERE cnpj_cpf = :cpf AND id_cadastro = :id_cadastro)

                UNION

		(SELECT nome, telefone, id_tipo_log, endereco, numero, complemento, cep, bairro,
                    cidade, uf, cpf, data_cadastro,CONCAT('funcio') AS tipo, rg, '', celular
                FROM base_web_control.funcionario
                WHERE cpf = :cpf1 AND id_cadastro = :id_cadastro1)

                UNION

                (SELECT fantasia AS nome, telefone, id_tipo_log AS id_tipo_log, endereco
                    , numero AS numero, complemento, cep, bairro, cidade, uf
                    , cnpj_cpf AS cpf, data_cadastro,CONCAT('fornec') AS tipo, rgie as rg, '', celular
                FROM base_web_control.fornecedor
                WHERE cnpj_cpf = :cpf2 AND id_cadastro = :id_cadastro2)

                ORDER BY data_cadastro DESC limit 1
	    ";

        // Preparando consulta SQl
        $pdo = $conexao->pdo->prepare($sql);

        // Criando array de campos e valores
        $aCamposValores = array(
            ':id_cadastro' => $_SESSION['USUARIO']['id_cadastro'],
            ':cpf' => $cpf_cnpj,
            ':id_cadastro1' => $_SESSION['USUARIO']['id_cadastro'],
            ':cpf1' => $cpf_cnpj,
            ':id_cadastro2' => $_SESSION['USUARIO']['id_cadastro'],
            ':cpf2' => $cpf_cnpj
        );

        // Substituindo valores e executando comando SQL
        $pdo->execute($aCamposValores);

        // Cria array contendo o resultado da consulta
        $aResult = $pdo->fetchAll(PDO::FETCH_ASSOC);

        // Verifico se existe algum registro na tabela, caso NAO pesquisa na Base Central
        if (!$aResult) {

            $sql1 = "SELECT
                            a.Nom_Nome as nome, b.Tipo_Log_id as id_tipo_log, b.logradouro as endereco
                            , b.numero as numero, b.complemento as complemento, b.cep as cep,
                            b.bairro as bairro, b.cidade as cidade, b.uf as uf, a.Nom_CPF as cpf, 'brasil',
                            c.Numero_RG as rg
                        FROM base_inform.Nome_Brasil a
                        LEFT OUTER JOIN base_inform.Endereco b on a.Nom_CPF = b.CPF
                        LEFT OUTER JOIN base_inform.Nome_RG c on a.Nom_CPF = c.CPF
                        WHERE a.Nom_CPF = :cpf
                        ORDER BY a.Origem_Nome_id, a.id, b.id DESC
                        LIMIT 1";

            $pdo1 = $conexao->pdo->prepare($sql1);
            $aCamposValores1 = array(
                ':cpf' => $cpf_cnpj
            );
            $pdo1->execute($aCamposValores1);
            $aResult = $pdo1->fetchAll(PDO::FETCH_ASSOC);
        }

        // Retorno
        return $aResult[0];
    }

    /**
     * Busca dados na tabela clientes, se nao achar busca na geral
     * @return dados
     */
    public static function dadosPorIdCliente($id_cliente, $id_cadastro)
    {

        $conexao = new dbConnection();
        $sql = "SELECT nome, telefone, id_tipo_log AS id_tipo_log
                    , endereco, numero, complemento, cep, bairro, cidade
                    , uf, cnpj_cpf AS cpf
                    , data_cadastro, 'client' AS tipo, rg, fone_empresa, celular
                FROM base_web_control.cliente
                WHERE id = $id_cliente AND id_cadastro = $id_cadastro
	    ";

        // Preparando consulta SQl
        $pdo = $conexao->pdo->prepare($sql);

        // Substituindo valores e executando comando SQL
        $pdo->execute();

        // Cria array contendo o resultado da consulta
        $aResult = $pdo->fetchAll(PDO::FETCH_ASSOC);

        // Retorno
        return $aResult[0];
    }

    public static function MesExtensoToMesNum($strmes)
    {
        /**
         * Array de de chaves (ingl�s) e valores (m�s em portugu�s)
         */
        $mes_extenso = array(
            'Janeiro' => '01',
            'Fevereiro' => '02',
            'Marco' => '03',
            'Abril' => '04',
            'Maio' => '05',
            'Junho' => '06',
            'Julho' => '07',
            'Agosto' => '08',
            'Setembro' => '09',
            'Outubro' => '10',
            'Novembro' => '11',
            'Dezembro' => '12'
        );
        /**
         * Retorn o m�s em portugu�s
         */
        return $mes_extenso [$strmes];
    }

    public static function formatarMascaraQtd($valor)
    {

        $arrValor = explode('|', $valor);

        $medida = empty($arrValor[1]) ? '' : trim($arrValor[1]);

        $arrValorQuebrado = explode(".", $arrValor[0]);
        $decimais = empty($arrValorQuebrado[1]) ? '' : $arrValorQuebrado[1];

        $valorFormatado = floatval($arrValor[0]);

        //return $medida;
        $strValorUnidade = '';

        switch ($medida) {

            case 'KG':

                $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;

                break;

            case 'UNID':
                @$strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }


                break;

            case 'PACOTE':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'CX':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'M':

                $strValorUnidade = number_format($valorFormatado, 2, ',', '.') . '|' . $medida;

                break;

            case 'PAR':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'KIT':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'LITRO':

                $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;

                break;

            case 'GRAMAS':

                $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;

                break;

            case 'OU':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'MG':

                $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;

                break;

            case 'M2':

                $strValorUnidade = number_format($valorFormatado, 2, ',', '.') . '|' . $medida;

                break;

            case 'M3':

                $strValorUnidade = number_format($valorFormatado, 2, ',', '.') . '|' . $medida;

                break;

            case 'DI':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'TON':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;

            case 'K':

                $strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;
            default :

                @$strValorUnidade = number_format($valorFormatado, 0, ',', '.') . '|' . $medida;
                if ($decimais != 0) {
                    $strValorUnidade = number_format($valorFormatado, 3, ',', '.') . '|' . $medida;
                }

                break;
        }

        return $strValorUnidade;
    }

    public static function BuscaNomeEmpresasFusao($idsEmpresas)
    {

        $empresas = '';

        if (strpos($idsEmpresas, ',') > 0) {
            include_once('../../controller/webcontrol/EmpresaController.class.php');
            include_once('../../model/webcontrol/EmpresaModel.class.php');
            include_once('../../classes/DbConnection.class.php');

            $empresa = new EmpresaController();

            $ids = explode(',', $idsEmpresas);

            foreach ($ids AS $key => $row) {
                $empresa->setCodloja($row);

                $dados = $empresa->buscaSimplificadaEmpresa();

                $empresas .= '|' . $dados['codloja'] . ';' . (trim($dados['razaosoc']) == '' ? $dados['nomefantasia'] : $dados['razaosoc']) . ' - ' . $dados['end_completo'] . $dados['cidade_completo'];
            }
        }

        return $empresas;

    }

    public static function AlteraSqlparaIN($campo, $sql)
    {
       // echo '<pre>';
       // print_r($_SESSION['FILIACAO']);
       // die;
        if (isset($_SESSION['FILIACAO'])) {
            if ($_SESSION['FILIACAO'][0]['tfiliacao'] == 2) {
                $in = ' IN(';

                $sql = str_replace('= :' . $campo, '=:' . $campo, $sql);


                for ($i = 0; $i < count($_SESSION['FILIACAO']); $i++) {
                    if (!isset($_SESSION['FILIACAO'][$i]['filiais'])) {
                        $in .= $_SESSION['FILIACAO'][$i]['id_cadastro'] . ',';
                    } else {
                        $in .= $_SESSION['FILIACAO'][$i]['filiais'] . ',';
                    }
                }

                $in = substr(str_replace(';', ',', $in), 0, -1) . ')';
                $in = str_replace(',,', ',', $in);
                
                if($in == ' IN()'){
                    $in = ' = '. $_SESSION['USUARIO']['id_cadastro'];
                }

                $campo = '=:' . $campo;

                // print_r(str_replace($campo, $in, $sql));die;

                return str_replace($campo, $in, $sql);
            } else {
                return $sql;
            }
        } else {
            return $sql;
        }
    }

    public static function AlteraArrayparaIN($arrayValores)
    {
        //var_dump($_SESSION);
        //echo $_GET['filial'].'+++'.$_SERVER['HTTP_REFERER'];
        $valores = '';
        //verifica se existe filiacao e se e do tipo 2(fusao)
        if (isset($_SESSION['FILIACAO'])) {
            if ($_SESSION['FILIACAO'][0]['tfiliacao'] == 1) {
                //se for do tipo 1, verifica se a pagina ativa é de filial
                if (isset($_GET['filial'])) {
                    $idFilial = $_GET['filial'];

                    if (Funcoes::BuscaFilialNaSessao($idFilial) != false) {
                        return $idFilial;
                    } else {
                        return $arrayValores;
                    }
                } else {
                    if (strrpos($_SERVER['HTTP_REFERER'], 'filial') != false) {
                        $urlFilial = substr($_SERVER['HTTP_REFERER'], (strrpos($_SERVER['HTTP_REFERER'], '=') + 1));
                        if (Funcoes::BuscaFilialNaSessao($urlFilial) != false) {
                            return $urlFilial;
                        } else {
                            return $arrayValores;
                        }
                    } else {
                        return $arrayValores;
                    }
                }
            } else {

                $arrayValores = Funcoes::AlteraSqlparaIN('id_cadastro', 'id_cadastro =:id_cadastro');

                return str_replace(')', '', substr(trim($arrayValores), 16));
            }
        } else {
            return $arrayValores;
        }
    }

    // verifica se o valor passado como filial, existe no array filiacao
    public static function BuscaFilialNaSessao($filial)
    {

        $retorno = false;

        if (isset($_SESSION['FILIACAO'])) {
            for ($i = 1; $i < count($_SESSION['FILIACAO']); $i++) {
                if (trim($_SESSION['FILIACAO'][$i]['id_cadastro']) == trim($filial)) {
                    $retorno = $i;
                }
            }
        } else {
            echo 'Sessao inexistente';
        }

        return $retorno;
    }

    public static function retornaNomeFornecedor($id_fornecedor)
    {

        require_once('../controller/webcontrol/FornecedorController.class.php');
        require_once('../model/webcontrol/FornecedorModel.class.php');

        $obj_fornecedor = new FornecedorController();
        $obj_fornecedor->setIntIdFornecedor($id_fornecedor);

        $nome_fornecedor = $obj_fornecedor->listarNomeFornecedor($id_fornecedor);

        return $nome_fornecedor;
    }

    public static function montaComboFuncionarios($id_cadastro)
    {

        require_once('../controller/webcontrol/FuncionarioController.class.php');
        require_once('../model/webcontrol/FuncionarioModel.class.php');

        $obj_funcionario = new FuncionarioController();
        $obj_funcionario->setIntIdCadastro($id_cadastro);

        $nome_funcionario = $obj_funcionario->montaComboFuncionarios($id_cadastro);

        return $nome_funcionario;
    }

    public static function formataPorcentagem($formataPorcentagem)
    {
        if ($formataPorcentagem) {
            $modificado = number_format($formataPorcentagem, 2, ',', '.') . '%';
            return $modificado;
        } else {
            if (floatval($formataPorcentagem) == 0 || intval($formataPorcentagem) == 0) {
                return '0%';
            }
        }
    }

    public static function truncate($valor, $format)
    {

        $valores = explode('.', $valor);

        $qtdDecimais = strlen(isset($valores[1]) ? $valores[1] : 0);

        if ($qtdDecimais > 2 && substr($valores[1], 2) > 0) {
            $k = floor($valor * 100) / 100;

            return number_format($k, $format);
        } else {
            return number_format($valor, $format);
        }
    }

    public static function truncateMoney($val, $f = 2)
    {
        if(($p = strpos($val, '.')) !== false) {
            $val = floatval(substr($val, 0, $p + 1 + 4));
        }
        return $val;
    }



    public static function validaEmail($email)
    {
        $conta = "/^[a-zA-Z0-9\._-]+@";
        $domino = "[a-zA-Z0-9\._-]+.";
        $extensao = "([a-zA-Z]{2,4})$/";
        $pattern = $conta . $domino . $extensao;
        if (preg_match($pattern, $email))
            return true;
        else
            return false;
    }

    public static function verificaUltimoDiaMes($mes)
    {

        $ano = date("Y"); // Ano atual
        $ultimo_dia = date("t", mktime(0, 0, 0, $mes, '01', $ano)); // Mágica, plim!

        return (int)$ultimo_dia;
    }


    public static function diaSemana($data)
    {

        $ano = substr("$data", 0, 4);
        $mes = substr("$data", 5, -3);
        $dia = substr("$data", 8, 9);

        $diasemana = date("w", mktime(0, 0, 0, $mes, $dia, $ano));

        switch ($diasemana) {
            case"0":
                $diasemana = "domingo";
                break;
            case"1":
                $diasemana = "segunda";
                break;
            case"2":
                $diasemana = "terça";
                break;
            case"3":
                $diasemana = "quarta";
                break;
            case"4":
                $diasemana = "quinta";
                break;
            case"5":
                $diasemana = "sexta";
                break;
            case"6":
                $diasemana = "sabado";
                break;
        }

        return "$diasemana";
    }
    
    
    
     /**
     * Registra LOG do envio de e-mails de orçamento 
     * @return bolean 
     * id do pedido, usuario que esta enviando o email, email do destinatario, pagina de origem do envio do email,
     */
    public static function grava_log_email_enviado($id_nota, $idcliente, $usuario_envio, $email_destino, $origem)
    {
// 		echo "INSERT INTO base_web_control.log_envio_email values('', $id_nota, $idcliente, $usuario_envio, '$email_destino', '$origem', NOW() )";exit;
    	$objConexao = new DbConnection();
        //$sql22 = "INSERT INTO base_web_control.log_envio_email values('', $id_nota, $idcliente, $usuario_envio, '$email_destino', '$origem', NOW())";
		//$sql = "INSERT INTO base_web_control.log_envio_email (id_venda, id_cliente, id_usuario_envio, email_destino, origem_envio, data_hora_envio) values($id_nota, $idcliente, $usuario_envio, '$email_destino', '$origem', NOW())";
		$sql22 = 'INSERT INTO base_web_control.log_envio_email (id_venda, id_cliente, id_usuario_envio, email_destino, origem_envio, data_hora_envio) values("'.$id_nota.'", "'.$idcliente.'", "'.$usuario_envio.'", "'.$email_destino.'", "'.$origem.'", NOW())';

        $pdo = $objConexao->pdo->prepare($sql22);
    	$pdo->execute();

    	return;
    }
    
    
    


}
