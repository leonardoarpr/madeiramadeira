FROM ubuntu

MAINTAINER Leonardo

RUN apt-get update && apt-get upgrade -y
RUN apt-get install apache2 -y
RUN apt-get install php-fpm -y

COPY madeiramadeira /var/www/html/madeiramadeira

CMD service apache start && service php start && /bin/bash
