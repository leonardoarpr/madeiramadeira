# madeiraMadeira
PARA A UTILIZAÇÂO DO PROGRAMA

Ter instalado apache >= 2.0 | PHP >= 5.6 | MYSQL > 6.0 | NODE > 7.0
e executar os scripts madeiramadeira.sql para o CRUD das tabelas

PARA A UTILIZAÇÃO DO NODE

1. Na pasta /bibliotecas/node/ instanciar no TERMINAL/CMD o comando node server

PARA A UTILIZAÇÂO DO REDIS EM AMBIENTE WINDOWS

2. Executar o arquivo /bibliotecas/redis/redis-win/redis-server.exe 

--- IMPORTANTE ---

Foi utilizado a engine do php para o node no arquivo /bibliotecas/node/server.js
caso queira utilizar o html descomentar a linha

//app.engine('html',require('ejs').renderFile);socket

--- PONTOS IMPORTANTES ---
visando as recomendações do manual do PHP a função __AUTOLOAD() que é depreciadana versão do PHP 7.2 foi substrituida pela versao mais recente do spl_autoload_register

instanciado o socket dentro de js/cadastroChamado.js, o mesmo se encontra rá dentro de suas respectivas funções
e retornará setando uma função.

  var socket = io('http://localhost:3000');

  socket.on('receivedMessage',function (id) {
      iniciarChat(id);
  });
 
 A classe redis foi associado ao repositorio app/ajax/cadastroChamado.php
 e utilizado nos controladores
 $redis = new redis();
  $redis->redis->set() ...