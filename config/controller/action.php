<?php
namespace config\controller;

abstract class action
{
	private $action;
	protected $view;

	public function __construct()
	{
		$this->view = new \stdClass();
	}

	public function index()
	{
		$this->render('index');
	}

	public function chamado()
	{
		$this->render('chamado');
	}

	protected function render($action,$layout = true)
	{
		include_once '../app/view/header.php';
		$this->action = $action;
		$this->content();

	}

	protected function content()
	{
		$current = get_class($this);
		$singleClassName = strtolower((str_replace('controller', '', str_replace('app\\controller\\','',$current))));
		// include_once '../app/view/'.$singleClassName.'/'$this->action.'.php';
		include_once '../app/view/'.$this->action.'.php';
	}
}