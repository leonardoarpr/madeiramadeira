<?php
namespace config\init;

class autoload
{

	private $arquivo;

	public function __construct()
	{
		spl_autoload_register([$this,'pasta']);
	}

	public function pasta($arquivo)
	{
		$this->arquivo = ['../../controller/'.$arquivo.'.php',
			              '../../app/model/'.$arquivo.'.php',
			              '../../app/ajax/'.$arquivo.'.php'
						];
		foreach ($this->arquivo as $v)
		{
			if(file_exists($v))
			{
				require_once $v;
				break;
			}
		}
	}
}
new autoload();
?>
